import React, { Component } from 'react';
import { Banner } from './Banner';
import { Row } from './Row';
import { Creator } from './Creator';
import { VisibilityControl } from './VisibilityControl';

export default class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      userName: 'Adam',
      items: [
        { action: 'Buy Flowers', done: false  },
        { action: 'Get Shoes', done: false  },
        { action: 'Collect Tickets', done: true  },
        { action: 'Call Joe', done: false  }
      ],
      showCompleted: true
    };
  }

  updateNewTextValue = (event) => {
    this.setState({ newItemText: event.target.value });
  }

  createNewItem = (task) => {
    if (!this.state.items.find(item => item.action === task)) {
      this.setState({
        items: [
          ...this.state.items,
          { action: task, done: false }
        ]
      }, () => localStorage.setItem('itemList', JSON.stringify(this.state)));
    }
  }

  toggle = (itm) => this.setState({ items: this.state.items.map(item => item.action === itm.action ? {...item, done: !item.done} : item) });

  tableRows = (doneValue) => this.state.items.filter(item => item.done === doneValue).map(item => 
    <Row key={item.action} item={item} callback={this.toggle} />);

  componentDidMount = () => {
    const data = localStorage.getItem("itemList");
    this.setState(data != null ? JSON.parse(data) : {
      userName: 'Adam',
      items: [
        { action: 'Buy Flowers', done: false  },
        { action: 'Get Shoes', done: false  },
        { action: 'Collect Tickets', done: true  },
        { action: 'Call Joe', done: false  }
      ],
      showCompleted: true 
    });
  }

  render() {
    return (
      <div>
        <Banner name={this.state.userName} tasks={this.state.items}/>
        <div className="container-fluid">
          <Creator callback={this.createNewItem}/>
          <table className="table table-striped table-bordered">
            <thead>
              <tr>
                <th>Description</th>
                <th>Done</th>
              </tr>
            </thead>
            <tbody>{this.tableRows(false)}</tbody>
          </table>

          <div class="bg-secondary text-white -text-center p-2">
            <VisibilityControl description="Completed Tasks" isChecked={this.state.showCompleted} callback={(checked) => this.setState({showCompleted: checked})}></VisibilityControl>
          </div>

          {this.state.showCompleted &&
            <table className="table table-striped table-bordered">
              <thead>
                <tr><th>Description</th><th>Done</th></tr>
              </thead>
              <tbody>{this.tableRows(true)}</tbody>
            </table>
          }
        </div>
      </div> 
    );
  }
}